# Intro

* This is very personal to my homelab but maybe someone can take some value from it.
* Inventory setup in `inventories/homelab/hosts.yml`, along with a group_vars in the same directory.
* Full explanation [on my blog](https://stef33560.gitlab.io/blog/2023/08/01/tftp)
  * Requirements :
    * a proxmox server, a master node
    * an LXC template inside (id [`{{ PROXMOX_VM_SRC_ID }}`](https://gitlab.com/ansible190/tftpxe/-/blob/main/inventories/homelab/group_vars/proxmox/vars.yml#L8)) made from in my case `debian-12-standard_12.0-1_amd64.tar.zst`

# What does it do ?

1. Supply a brand new container `PROXMOX_VM_DST_NAME` (see [`inventories/homelab/all.yml`](https://gitlab.com/ansible190/tftpxe/-/blob/main/inventories/homelab/group_vars/all.yml#L1)) wth 1 vCPU/512Mo Ram
1. Set 2 interfaces on the container :
   * eth0, dhcp on main network
   * net1, fixed ip on tagged vlan 2 (192.168.2.254/24, hardcoded see [task 'Create NIC net1'](https://gitlab.com/ansible190/tftpxe/-/blob/main/proxmox/tasks/main.yml#L36)) #TODO add a parameter and sets it on another vmbridge than vmbr0
1. Boot the VM
1. Deploy isc-dhcp-server (IPv4), iptables (and forwarding)
1. Deploy atftpd and setup a Debian Bookworm with an autoconfig install process

# Secrets

Put secrets in `inventories/homelab/(proxmox|pxe)/vault.yml{{ site_inventory }}.yml` like this :

```yml title='inventories/homelab/proxmox/vault.yml'
VAULT_PROXMOX_USER: "my super login"
VAULT_PROXMOX_USER: "my undefinable passw0rd"
```

```yml  title='inventories/homelab/pxe/vault.yml'
VAULT_PRESEED_ROOTPWD: "My wunderbard root password"
VAULT_PRESEED_USRPWD: "The default password of a user"
```

# Rock'n'roll

```bash
export ANSIBLE_VAULT_PASSWORD_FILE=/here/is/the/master/key
ansible-playbook -i inventories/homelab/hosts.yml pxe.yml
```
